#include<bits.h>
#include<stdc++.h>
using namespace std;

int n;
int arr[22], cnt[11] = {}, MAX = -1, cur = -1;

int main() {
    cout << "Nhap n: ";
    cin >> n;
    for(int i = 1; i <= n; i++) {
        do {
            cin >> arr[i];
        }
        while(arr[i] < 0 || arr[i] > 10);
        cnt[arr[i]] ++;
        if(MAX < cnt[arr[i]]) {
            cur = arr[i];
        }
    }
    cout << "Gia tri xuat hien nhieu nhat trong mang: " << cur;
}