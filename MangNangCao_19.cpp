#include<bits.h>
#include<stdc++.h>
using namespace std;

int n, p[22][22];

int main() {
    cout << "Nhap chieu cao tam giac Pascal:";
    cin >> n;
    for(int i = 1; i <= n; i++) {
        if(i == 1) {
            p[1][1] = 1;
        }
        else if(i == 2) {
            p[2][1] = 1;
            p[2][2] = 1;
        }
        else {
            p[i][1] = 1;
            p[i][i] = 1;
            for(int j = 2; j <= i-1; j++) {
                p[i][j] = p[i-1][j-1] + p[i-1][j];
            } 
        }
    }
    cout << "Tam giac Pascal chieu cao " << n << endl; 
    for(int i = 1; i <= n; i++) {
        for(int j = 1; j <= i; j++) {
            cout << p[i][j] << " ";
        }
        cout << endl;
    }
}

