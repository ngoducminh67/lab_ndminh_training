#include<bits.h>
#include<stdc++.h>
using namespace std;

float arr[4], maxs = -9999999, mins = 9999999;

int main() {
    for(int i = 0; i < 4; i++) {
        cin >> arr[i];
        if(maxs < arr[i]) maxs = arr[i];
        if(mins > arr[i]) mins = arr[i];
    }
    cout << "max: " << maxs << endl;
    cout << "min: " << mins << endl;
}